const forwarder = require("./forwarder.js");
const {parseResponse} = require("./parse-http-response.js");
const express = require("express");
const app = express();
const webPort = 3000;

app.get("/start/:userKey/:forwardHost", async (req, res) => {
	const userKey = req.params.userKey;
	if (forwarder.addUserKey(userKey, req.params.forwardHost)) {
		res.send({success: true});
	}
	else {
		res.send({success: false, message: `User key "${userKey}" is already in use`});
	}
});

app.get("/stop/:userKey", async (req, res) => {
	const userKey = req.params.userKey;
	if (forwarder.removeUserKey(userKey)) {
		res.send({success: true});
	}
	else {
		res.send({success: false, message: `User key "${userKey}" is NOT already in use`});
	}
});

app.post("/*", async (req, res) => {
	await process(req, res);
});

app.get("/*", async (req, res) => {
	await process(req, res);
});

app.listen(webPort, () => console.log("Listening to API King"));

async function process(req, res) {
	let requestTextArr = [`${req.method.toUpperCase()} ${req.url} HTTP/${req.httpVersion}\n`];
	let userKey = null;
	let currentHost = null;
	
	for (let i = 0; i < req.rawHeaders.length; i = i + 2) {
		if (req.rawHeaders[i] === "X-ApiForwarder") {
			userKey = req.rawHeaders[i + 1];
			continue;
		}
		else if (req.rawHeaders[i] === "Host") {
			currentHost = req.rawHeaders[i + 1];
			continue;
		}
		
		requestTextArr.push(`${req.rawHeaders[i]}: ${req.rawHeaders[i + 1]}\n`);
	}
	
	if (!userKey) {
		res.status(403);
		res.send({success: false, message: "No user key sent in header"});
		return;
	}
	
	const userEntry = forwarder.getUserEntry(userKey);
	if (!userEntry) {
		res.status(403);
		res.send({success: false, message: `Invalid user key in header: ${userKey}`});
	}
	
	requestTextArr.push(`Host: ${userEntry.forwardHost}\n`);
	requestTextArr.push("\n");
	
	let finishedReading = false;
	
	req.on("readable", () => {
		let chunk;
		while ((chunk = req.read()) !== null) {
			requestTextArr.push(chunk);
		}
		finishedReading = true;
	});
	
	const p = new Promise(async (resolve, reject) => {
		while (!finishedReading) {
			// prevent infinite loop that blocks the thread
			await new Promise(r => setTimeout(r, 10));
		}
		
		let response = "";
		let receivedResponse = false;
		
		function callback(data, isComplete) {
			response += data;
			receivedResponse = isComplete;
			console.log("received callback: " + data.length + " - " + isComplete);
		}
		
		if (!forwarder.transmit(requestTextArr.join(""), userKey, callback)) {
			reject("Could not transmit");
			res.sendStatus(500);
		}
		
		while (!receivedResponse) {
			// prevent infinite loop that blocks the thread
			await new Promise(r => setTimeout(r, 10));
		}
		
		resolve(response);
	})
	.catch((reason) => {
		console.log("Promise rejected: " + reason);
		res.sendStatus(500);
	});
	
	const finalResponseText = await p;
	const data = parseResponse(Buffer.from(finalResponseText), {decodeContentEncoding: true})
	
	for (const existingHeader of res.getHeaderNames()) {
		res.removeHeader(existingHeader);
	}
	
	for (const key in data.rawHeaders) {
		if (key === "Transfer-Encoding") {
			continue;
		}
		res.setHeader(key, data.rawHeaders[key]);
	}
	
	res.status(data.statusCode);
	res.send(data.bodyData);
}

// export app to be used when testing
module.exports = app;