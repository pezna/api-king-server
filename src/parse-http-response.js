const zlib = require("zlib");

function parseChunked(rawChunkedData) {
	if (!Buffer.isBuffer(rawChunkedData)) {
		throw new Error("parseChunked needs to be given a buffer");
	}

	const bodyDataSlices = [];
	let pos = 0;
	while (true) {
		if (pos >= rawChunkedData.length) {
			break;
		}

		// Find next CRLF
		let i = rawChunkedData.indexOf("\r\n", pos);
		if (i === -1) {
			throw new Error("Invalid chunked encoding: can't find a CRLF");
		}
		
		if (i <= pos) {
			throw new Error("Invalid chunked encoding: got CRLF too soon");
		}

		const chunkLengthText = rawChunkedData.slice(pos, i).toString("utf8");

		if (!chunkLengthText.match(/^[0-9a-f]+$/)) {
			throw new Error("Invalid bytes for chunk length");
		}
		
		const chunkLength = parseInt(chunkLengthText, 16);
		if (chunkLength === 0) {
			break;
		}

		const chunkData = rawChunkedData.slice(i + 2, i + 2 + chunkLength);
		bodyDataSlices.push(chunkData);
		console.log("chunked slice");
		console.log(chunkData.toString());
		
		pos = i + 2 + chunkLength;
		const nextPos = rawChunkedData.indexOf("\r\n", pos);
		if (nextPos === -1) {
			break;
		}
		pos = nextPos + 2;
	}
	
	return Buffer.concat(bodyDataSlices);
}

function parseResponse(data, opt) {
	if ((typeof opt.decodeContentEncoding) !== "boolean") {
		throw new Error("Option must be specified: decodeContentEncoding");
	}
	if (!Buffer.isBuffer(data)) {
		throw new Error("Expected a Buffer");
	}

	// Split into (head, body)
	const splitIndex = data.indexOf("\r\n\r\n");
	if (splitIndex === -1) {
		throw new Error("Data does not contain CRLFCRLF");
	}
	const headData = data.slice(0, splitIndex);
	const rawBodyData = data.slice(splitIndex + 4);
	const headText = headData.toString("ascii");
	const headLines = headText.split("\r\n");

	// Assert the head data is ASCII
	// (because `.toString("ascii")` strips the high bit instead of erroring)
	const headData2 = Buffer.from(headText, "ascii");
	if (Buffer.compare(headData, headData2) !== 0) {
		throw new Error("Expected response head to be ASCII");
	}

	// First line
	const firstLine = headLines[0];
	const m = firstLine.match(/^HTTP\/[12](?:\.[01])? ([0-9]{3}) (.*)$/);
	if (!m) {
		throw new Error("Invalid status line");
	}
	const statusCode = parseInt(m[1], 10);
	const statusMessage = m[2];

	// Headers
	const headers = {};
	const rawHeaders = {};
	for (const line of headLines.slice(1)) {
		const i = line.indexOf(": ");
		if (i === -1) {
			throw new Error("Header line does not contain ': '");
		}
		const k = line.substr(0, i);
		const v = line.substr(i + 2);
		headers[k.toLowerCase()] = v;
		rawHeaders[k] = v;
	}

	let bodyData;

	const contentLengthText = headers["content-length"];
	if (contentLengthText) {
		if (!contentLengthText.match(/^[1-9][0-9]*$/)) {
			throw new Error("Content-Length does not match /^[1-9][0-9]*$/");
		}
		const contentLength = parseInt(contentLengthText, 10);
		if (contentLength != rawBodyData.length) {
			throw new Error("Content-Length does not match the length of the body data we have");
		}
		bodyData = rawBodyData;
		} else {
		if (headers["transfer-encoding"] != "chunked") {
			throw new Error("We need Content-Length or 'Transfer-Encoding: chunked'");
		}
		bodyData = parseChunked(rawBodyData);
	}

	if (opt.decodeContentEncoding && headers.hasOwnProperty("content-encoding")) {
		if (headers["content-encoding"] !== "gzip") {
			throw new Error("Unsupported Content-Encoding");
		}
		bodyData = zlib.gunzipSync(bodyData);
	}

	return {statusCode, statusMessage, headers, rawHeaders, bodyData};
}

module.exports = {
	parseResponse
};