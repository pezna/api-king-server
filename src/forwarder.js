const net = require("net");
const socketPort = 5000;
const beginMarker = Buffer.from("@@-BEGIN-@@");
const endMarker = Buffer.from("@@-END-@@");
const userMarker = Buffer.from("@@-USER-@@");

const currentUserKeys = new Map();
const socketKeyToUserKey = new Map();

function getSocketKey(socket) {
	return `${socket.localAddress}-${socket.localPort}-${socket.remoteAddress}-${socket.remotePort}`;
}

function cacheSocket(socket, userKey) {
	socketKeyToUserKey.set(socket.socketKey, userKey);
	
	const userEntry = currentUserKeys.get(userKey);
	if (!userEntry) {
		throw `No user entry for key: ${userKey}`;
	}
	userEntry.socket = socket;
}

function uncacheSocket(socket) {
	const userKey = socketKeyToUserKey.get(socket.socketKey);
	socketKeyToUserKey.delete(socket.socketKey);
	
	removeUserKey(userKey);
}

function addUserKey(userKey, forwardHost) {
	if (currentUserKeys.has(userKey)) {
		return false;
	}
	
	currentUserKeys.set(userKey, {
		socket: null,
		callback: null,
		data: null,
		forwardHost
	});
	return true;
}

function removeUserKey(userKey) {
	if (!currentUserKeys.has(userKey)) {
		return false;
	}
	
	const userEntry = currentUserKeys.get(userKey);
	if (userEntry.socket) {
		userEntry.socket.end();
		userEntry.socket = null;
	}
	currentUserKeys.delete(userKey);
	
	return true;
}

function getUserEntry(userKey) {
	if (!currentUserKeys.has(userKey)) {
		return false;
	}
	
	return currentUserKeys.get(userKey);
}

function handleData(socket, data) {
	let dataStartOffset = 0;
	let dataEndOffset = 0;
	do {
		let obtainedCompleteTransmission = false;
		
		if (data.slice(0, beginMarker.length).equals(beginMarker)) {
			dataStartOffset = beginMarker.length;
		}
		
		if (!socket.hasOwnProperty("userKey")) {
			throw "Socket should have user key set";
		}
		
		dataEndOffset = data.indexOf(endMarker, dataStartOffset);
		if (dataEndOffset < 0) {
			dataEndOffset = data.length;
		}
		else {
			obtainedCompleteTransmission = true;
		}
		
		if (dataEndOffset > dataStartOffset) {
			const msg = data.slice(dataStartOffset, dataEndOffset);
			const userEntry = currentUserKeys.get(socket.userKey);
			userEntry.callback(msg, obtainedCompleteTransmission);
		}
		
		data = data.slice(dataEndOffset + endMarker.length);
	}
	while (data.length > 0);
}

function transmit(text, userKey, callback) {
	if (!currentUserKeys.has(userKey)) {
		console.log("USER KEY DOES NOT EXIST: " + userKey);
		return false;
	}
	
	const userEntry = currentUserKeys.get(userKey);
	userEntry.callback = callback;
	
	const isKernelBufferFull = userEntry.socket.write(beginMarker + text + endMarker);
	if (isKernelBufferFull) {
		console.log("Data was flushed successfully from kernel buffer i.e written successfully!");
	}
	else {
		userEntry.socket.pause();
	}
	
	return true;
}

const server = net.createServer();

server.on("close", function() {
	console.log("Socket server closed");
});

server.on("connection", function(socket) {
	console.log("Buffer size: " + socket.bufferSize);
	console.log("---------------- server details ----------------");
	let address = server.address();
	console.log("Server IP: " + address.address);
	console.log("Server port: " + address.port);
	console.log("Server is IPv4/IPv6: " + address.family);
	
	let localAddress = socket.localAddress;
	let localPort = socket.localPort;
	console.log("Server LOCAL IP: " + localAddress);
	console.log("Server LOCAL port: " + localPort);
	
	console.log("---------------- remote client details ----------------");
	
	console.log("REMOTE Socket IP: " + socket.remoteAddress);
	console.log("REMOTE Socket port: " + socket.remotePort);
	console.log("REMOTE Socket is IPv4/IPv6: " + socket.remoteFamily);
	
	console.log("------------------------------------------");
	
	server.getConnections((error, count) => {
		console.log("Number of concurrent connections to the server: " + count);
	});
	
	socket.setTimeout(900000, () => {
		console.log("Socket timed out");
	});
	
	// add property for socket key to the socket itself for speed
	socket.socketKey = getSocketKey(socket);
	
	socket.on("data", function(data) {
		console.log("-- Received Data --");
		console.log("Bytes read: " + socket.bytesRead);
		console.log("Bytes written: " + socket.bytesWritten);
		//console.log("Data sent to server: " + data);
		//console.log("Data length sent to server: " + data.length);
		
		if (data.slice(0, userMarker.length).equals(userMarker)) {
			const userKeyLength = data[userMarker.length];
			const userKey = data.slice(userMarker.length + 1, userMarker.length + 1 + userKeyLength).toString();
			
			console.log("extracted user key: " + userKey);
			if (!currentUserKeys.has(userKey)) {
				throw `User key has not been registered: ${userKey}`;
			}
			
			if (socket.hasOwnProperty("userKey")) {
				if (userKey != socket.userKey) {
					throw `Expected user key to be ${socket.userKey} not ${userKey}`;
				}
			}
			else {
				socket.userKey = userKey;
				cacheSocket(socket, userKey);
			}
		}
		else {
			handleData(socket, data);
		}
	});
	
	socket.on("drain", function() {
		console.log("write buffer is empty now .. u can resume the writable stream");
		socket.resume();
	});
	
	socket.on("error", function(error) {
		console.log("Error: " + error);
	});
	
	socket.on("timeout", function() {
		console.log("Socket timed out!");
		socket.end();
	});
	
	socket.on("close", function(error) {
		console.log("-- Socket closed --");
		console.log("Bytes read: " + socket.bytesRead);
		console.log("Bytes written: " + socket.bytesWritten);
		
		uncacheSocket(socket);
		
		if (error) {
			console.log("Socket closed because of transmission error");
		}
	});
});

server.listen(socketPort);

module.exports = {
	addUserKey,
	removeUserKey,
	getUserEntry,
	transmit
}